include ${FSLCONFDIR}/default.mk

PROJNAME = mm
XFILES   = mm
SOFILES  = libfsl-mm.so
LIBS     = -lfsl-newimage -lfsl-miscmaths -lfsl-utils -lfsl-miscplot \
           -lfsl-miscpic -lfsl-NewNifti -lfsl-znz  -lfsl-cprob -lgdc \
           -lgd -lpng

all: mm libfsl-mm.so

libfsl-mm.so: mmoptions.o mixture_model.o
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

mm: mm.o libfsl-mm.so
	${CXX} ${CXXFLAGS} -o $@ $^ -lfsl-mm ${LDFLAGS}
