/*  mixture_model.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 2004 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(mixture_model_h)
#define mixture_model_h

#include <iostream>
#include <fstream>
#include <vector>
#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "utils/tracer_plus.h"
#include "miscmaths/sparse_matrix.h"
#include "miscmaths/minimize.h"
#include "cprob/libprob.h"
#include "mmoptions.h"
#include "connected_offset.h"

namespace Mm{

  inline double boundexp(double x)
  {
    double bound=700;
    if(x>bound) x=bound;
    else if(x<-bound) x=-bound;
    double ret = std::exp(x);

    return ret;
  }

  class Distribution
  {
  public:
    Distribution() : useprop(false){}
    virtual float pdf(float val) const = 0;
    virtual float dpdfdmn(float val) const = 0;
    virtual float dpdfdvar(float val) const  = 0;

    virtual ~Distribution(){}

    float getmean() const {return mn;}
    float getvar() const {return var;}
    float getprop() const {return prop;}

    virtual bool setparams(float pmn, float pvar, float pprop){mn=pmn;var=pvar;prop=pprop;return true;}
    void setuseprop(bool puseprop) {useprop = puseprop;}

  protected:
    float mn;
    float var;
    float prop;
    float useprop;
  };

  class GaussianDistribution : public Distribution
  {
  public:

    GaussianDistribution() : Distribution() {
    }

    virtual float pdf(float val) const {
      float ret = premult*std::exp(-0.5/var*MISCMATHS::Sqr(val-mn));
      if(useprop) ret *= prop;
      return ret;
    }

    virtual float dpdfdmn(float val) const {
      float ret = premult*(val-mn)/var*std::exp(-0.5/var*MISCMATHS::Sqr(val-mn));
      return ret;
    }

    virtual float dpdfdvar(float val) const {
      float ret = premult*0.5*(MISCMATHS::Sqr(val-mn)-var)/std::pow(var,2)*std::exp(-0.5/var*MISCMATHS::Sqr(val-mn));

      return ret;
    }

    virtual ~GaussianDistribution(){}

    virtual bool setparams(float pmean, float pvar, float pprop)
    {
      Distribution::setparams(pmean, pvar, pprop);

      if(pvar<=0)
        return false;

      useprop ? premult=pprop/std::sqrt(2*M_PI*pvar) : premult=1.0/std::sqrt(2*M_PI*pvar);

      return true;
    }

  private:
    float premult;

  };

  class GammaDistribution : public Distribution
  {
  public:
    GammaDistribution(float pminmode = 0) : Distribution(), minmode(pminmode) {}

    virtual float pdf(float val) const
    {
      float ret = 1e-32;
      if(val > 0){
        ret = boundexp(preadd + (a-1) * std::log(val) - b*val);
        if(useprop) ret *= prop;
      }

      return ret;
    }

    virtual float dpdfdmn(float val) const {

      float ret = 0;

      if(val > 0)
        ret = dpdfda(val)*(2*mn/var)+dpdfdb(val)*(1/var);

      return ret;
    }

    virtual float dpdfdvar(float val) const {

      float ret = 0;

      if(val > 0)
        ret = dpdfda(val)*(-MISCMATHS::Sqr(mn)/MISCMATHS::Sqr(var))+dpdfdb(val)*(-mn/MISCMATHS::Sqr(var));

      return ret;
    }

    virtual ~GammaDistribution(){}

    virtual bool setparams(float pmn, float pvar, float pprop) {
      Distribution::setparams(pmn, pvar, pprop);
      bool ret = validate();

      a = std::pow(mn,2)/var;
      b = mn/var;

      useprop ? preadd= log(prop)+a*std::log(b)-CPROB::lgam(a) : preadd=a*std::log(b)-CPROB::lgam(a);
      useprop ? premult=pprop/std::sqrt(2*M_PI*pvar) : premult=1.0/std::sqrt(2*M_PI*pvar);
      digama = MISCMATHS::digamma(a);

      if(!ret) std::cout << "invalid gamma" << std::endl;
      return ret;
    }

    void setminmode(float pminmode) {minmode=pminmode; validate();}

  private:

    float dpdfdb(float val) const {
      return std::pow(b,a-1)*std::pow(val,a-1)/std::exp(CPROB::lgam(a))*std::exp(-b*val)*(a-b*val);
    }

    float dpdfda(float val) const {
      return std::pow(b,a)*std::pow(val,a-1)/std::exp(CPROB::lgam(a))*std::exp(-b*val)*(std::log(b)+std::log(val)-digama);
    }

    bool validate();

    float digama;
    float preadd;
    float a;
    float b;
    float minmode;
    float premult;

  };

  class FlippedGammaDistribution : public Distribution
  {
  public:
    FlippedGammaDistribution(float pminmode = 0) : Distribution(), minmode(pminmode) {}

    virtual float pdf(float val) const
    {
      float ret = 1e-32;
      val = -val;

      if(val > 0) {
        ret = boundexp(preadd + (a-1) * std::log(val) - b*val);
        //ret = boundexp(preadd + (a-1) * std::log(std::abs(val)) - b*std::abs(val));

        if(useprop) ret *= prop;
      }
      return ret;
    }

    virtual float dpdfdmn(float val) const {

      // flip val
      val = -val;
      float pmn = -mn;

      float ret = 0;
      if(val > 0)
        ret = dpdfda(val)*(2*pmn/var)+dpdfdb(val)*(1/var);

      return -ret;
    }

    virtual float dpdfdvar(float val) const {

      // flip val
      val = -val;
      float pmn = -mn;

      float ret = 0;

      if(val > 0)
        ret = dpdfda(val)*(-MISCMATHS::Sqr(pmn)/MISCMATHS::Sqr(var))+dpdfdb(val)*(-pmn/MISCMATHS::Sqr(var));

      return ret;
    }

    virtual ~FlippedGammaDistribution(){}

    virtual bool setparams(float pmn, float pvar, float pprop)
    {
      Distribution::setparams(pmn, pvar, pprop);
      bool ret = validate();

      a = std::pow(mn,2)/var;
      b = -(mn)/var;
      useprop ? preadd= log(prop)+a*std::log(b)-CPROB::lgam(a) : preadd=a*std::log(b)-CPROB::lgam(a);
      useprop ? premult=pprop/std::sqrt(2*M_PI*pvar) : premult=1.0/std::sqrt(2*M_PI*pvar);
      digama = MISCMATHS::digamma(a);

      if(!ret) std::cout << "invalid gamma" << std::endl;
      return ret;
    }

    void setminmode(float pminmode) {minmode=pminmode; validate();}

  private:
    bool validate();

    float dpdfdb(float val) const {
      return std::pow(b,a-1)*std::pow(val,a-1)/std::exp(CPROB::lgam(a))*std::exp(-b*val)*(a-b*val);
    }

    float dpdfda(float val) const {
      return std::pow(b,a)*std::pow(val,a-1)/std::exp(CPROB::lgam(a))*std::exp(-b*val)*(std::log(b)+std::log(val)-digama);
    }

    float digama;
    float preadd;
    float a;
    float b;
    float minmode;
    float premult;
  };

  class SmmVoxelFunction : public MISCMATHS::EvalFunction
  {
  public:
    SmmVoxelFunction(float pdata, std::vector<Distribution*>& pdists, float plambda, float plog_bound)
      : MISCMATHS::EvalFunction(),
        data(pdata),
        dists(pdists),
        nclasses(pdists.size()),
        lambda(plambda),
        log_bound(plog_bound)
    {}

    float evaluate(const NEWMAT::ColumnVector& x) const; //evaluate the function

    virtual ~SmmVoxelFunction(){};

  private:
    SmmVoxelFunction();
    const SmmVoxelFunction& operator=(SmmVoxelFunction& par);
    SmmVoxelFunction(const SmmVoxelFunction&);

    float data;
    std::vector<Distribution*>& dists;
    int nclasses;
    float lambda;
    float log_bound;
  };

  class SmmFunction : public MISCMATHS::gEvalFunction
  {
  public:
    SmmFunction(const NEWMAT::ColumnVector& pdata, std::vector<Distribution*>& pdists, const float& pmrf_precision, const NEWIMAGE::volume<int>& pmask, const std::vector<Connected_Offset>& pconnected_offsets, const NEWIMAGE::volume<int>& pindices, const MISCMATHS::SparseMatrix& pD, float plambda, float plog_bound);

    float evaluate(const NEWMAT::ColumnVector& x) const; //evaluate the function

    NEWMAT::ReturnMatrix g_evaluate(const NEWMAT::ColumnVector& x) const; //evaluate the gradient function

    virtual ~SmmFunction(){};

  private:
    SmmFunction();
    const SmmFunction& operator=(SmmFunction& par);
    SmmFunction(const SmmFunction&);

    const NEWMAT::ColumnVector& data;
    std::vector<Distribution*>& dists;
    const float& mrf_precision;
    const NEWIMAGE::volume<int>& mask;
    const std::vector<Connected_Offset>& connected_offsets;
    const NEWIMAGE::volume<int>& indices;
    const MISCMATHS::SparseMatrix& D;

    int num_superthreshold;
    int nclasses;
    float lambda;
    float log_bound;

  };

  class SmmFunctionDists : public MISCMATHS::gEvalFunction
  //class SmmFunctionDists : public EvalFunction
  {
  public:
    SmmFunctionDists(const NEWMAT::ColumnVector& pdata, std::vector<Distribution*>& pdists, const float& pmrf_precision, const NEWIMAGE::volume<int>& pmask, const std::vector<Connected_Offset>& pconnected_offsets, const NEWIMAGE::volume<int>& pindices, float plambda, float plog_bound, const NEWMAT::ColumnVector& m_tildew);

    float evaluate(const NEWMAT::ColumnVector& x) const; //evaluate the function

    NEWMAT::ReturnMatrix g_evaluate(const NEWMAT::ColumnVector& x) const; //evaluate the gradient function

    virtual ~SmmFunctionDists(){};

  private:
    SmmFunctionDists();
    const SmmFunctionDists& operator=(SmmFunctionDists& par);
    SmmFunctionDists(const SmmFunctionDists&);

    const NEWMAT::ColumnVector& data;
    std::vector<Distribution*>& dists;
    const float& mrf_precision;
    const NEWIMAGE::volume<int>& mask;
    const std::vector<Connected_Offset>& connected_offsets;
    const NEWIMAGE::volume<int>& indices;
    std::vector<NEWMAT::RowVector> w;

    int num_superthreshold;
    int nclasses;
    float lambda;
    float log_bound;

    const NEWMAT::ColumnVector& m_tildew;
  };

  class Mixture_Model
  {
  public:

    // Constructor
    Mixture_Model(const NEWIMAGE::volume<float>& pspatial_data, const NEWIMAGE::volume<int>& pmask, const NEWIMAGE::volume<float>& pepi_example_data, float pepibt, std::vector<Distribution*>& pdists, std::vector<NEWIMAGE::volume<float> >& pw_means, NEWMAT::ColumnVector& pY, MmOptions& popts);

    Mixture_Model(const NEWIMAGE::volume<float>& pspatial_data, const NEWIMAGE::volume<int>& pmask, const NEWIMAGE::volume<float>& pepi_example_data, float pepibt, std::vector<Distribution*>& pdists, std::vector<NEWIMAGE::volume<float> >& pw_means, NEWMAT::ColumnVector& pY, bool pnonspatial=false, int pniters=10, bool pupdatetheta=true, int pdebuglevel=0, float pphi=0.015, float pmrfprecstart=10.0, int pntracesamps=10, float pmrfprecmultiplier=10.0, float pinitmultiplier=6.0, bool pfixmrfprec=false);

    // setup
    void setup();

    // run
    void run();

    // save data to logger dir
    void save() ;

    // Destructor
    virtual ~Mixture_Model(){}

  private:

    Mixture_Model();
    const Mixture_Model& operator=(Mixture_Model&);
    Mixture_Model(Mixture_Model&);

    void update_theta();
    void update_mrf_precision();
    void update_tildew_scg();
    void update_voxel_tildew_vb();
    void calculate_taylor_lik();
    void calculate_trace_tildew_D();

    void get_weights(std::vector<NEWMAT::ColumnVector>& weights, const NEWMAT::ColumnVector& pmtildew);

    void get_weights2(std::vector<NEWMAT::ColumnVector>& weights, std::vector<std::vector<std::vector<float> > >& weights_samps, std::vector<std::vector<std::vector<float> > >& tildew_samps, int nsamps, const NEWMAT::ColumnVector& pmtildew);

    void save_weights(const NEWMAT::ColumnVector& pmtildew, const char* affix, bool usesamples = true);

    int xsize;
    int ysize;
    int zsize;
    int num_superthreshold;
    int nclasses;

    const NEWIMAGE::volume<float>& spatial_data;
    const NEWIMAGE::volume<int>& mask;
    const NEWIMAGE::volume<float>& epi_example_data;
    float epibt;

    NEWIMAGE::volume4D<float> localweights;
    std::vector<Connected_Offset> connected_offsets;

    NEWIMAGE::volume<int> indices;

    NEWMAT::ColumnVector& Y;
    MISCMATHS::SparseMatrix D;

    NEWMAT::ColumnVector m_tildew;
    std::vector<NEWMAT::SymmetricMatrix> prec_tildew;
    std::vector<NEWMAT::SymmetricMatrix> cov_tildew;

    MISCMATHS::SparseMatrix precision_lik;
    NEWMAT::ColumnVector derivative_lik;

    float mrf_precision;
    //      float mrf_precision_old;

    bool nonspatial;
    int niters;
    bool stopearly;

    bool updatetheta;
    int debuglevel;

    // logistic transform params:
    float lambda;
    float log_bound;

    float trace_covariance_tildew_D;

    int it;

    std::vector<Distribution*>& dists;
    std::vector<NEWIMAGE::volume<float> >& w_means;

    int ntracesamps;
    float mrfprecmultiplier;
    float initmultiplier;
    bool fixmrfprec;

    float trace_tol;
    float scg_tol;

    std::vector<float> meanhist;
    std::vector<float> mrf_precision_hist;
  };

  NEWMAT::ReturnMatrix sum_transform(const NEWMAT::RowVector& wtilde, float log_bound);
  NEWMAT::ReturnMatrix logistic_transform(const NEWMAT::RowVector& wtilde,float lambda,float log_bound);
  NEWMAT::ReturnMatrix inv_transform(const NEWMAT::RowVector& w,float lambda,float log_bound,float initmultiplier);

  void ggmfit(const NEWMAT::RowVector& data, std::vector<Distribution*>& pdists, bool useprops);
  void plot_ggm(const std::vector<NEWIMAGE::volume<float> >& w_means, const std::vector<Distribution*>& dists, const NEWIMAGE::volume<int>& mask, const NEWMAT::ColumnVector& Y);

  void make_ggmreport(const std::vector<NEWIMAGE::volume<float> >& w_means, const std::vector<Distribution*>& dists, const NEWIMAGE::volume<int>& mask, const NEWIMAGE::volume<float>& spatial_data, bool zfstatmode, bool overlay, const NEWIMAGE::volume<float>& epivol, float thresh, bool nonspatial, bool updatetheta, const std::string& data_name);
  void calculate_props(const std::vector<NEWIMAGE::volume<float> >& w_means, std::vector<Distribution*>& dists, const NEWIMAGE::volume<int>& mask);
}
#endif
