/*  mmoptions.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(mmoptions_h)
#define mmoptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

namespace Mm {

class MmOptions {
 public:
  static MmOptions& getInstance();
  ~MmOptions() { delete gopt; }

  Utilities::Option<bool> verbose;
  Utilities::Option<int> debuglevel;
  Utilities::Option<bool> timingon;
  Utilities::Option<bool> help;
  Utilities::Option<std::string> spatialdatafile;
  Utilities::Option<std::string> epiexampledatafile;
  Utilities::Option<std::string> maskfile;
  Utilities::Option<std::string> logdir;
  Utilities::Option<bool> nonspatial;
  Utilities::Option<bool> fixmrfprec;
  Utilities::Option<float> mrfprecstart;
  Utilities::Option<float> mrfprecmultiplier;
  Utilities::Option<float> initmultiplier;
  Utilities::Option<bool> updatetheta;
  Utilities::Option<bool> zfstatmode;
  Utilities::Option<float> phi;
  Utilities::Option<int> niters;
  Utilities::Option<float> threshold;

  void parse_command_line(int argc, char** argv, Utilities::Log& logger);

 private:
  MmOptions();
  const MmOptions& operator=(MmOptions&);
  MmOptions(MmOptions&);

  Utilities::OptionParser options;

  static MmOptions* gopt;

};

 inline MmOptions& MmOptions::getInstance(){
   if(gopt == NULL)
     gopt = new MmOptions();

   return *gopt;
 }

 inline MmOptions::MmOptions() :
   verbose(std::string("-V,--verbose"), false,
	   std::string("switch on diagnostic messages"),
	   false, Utilities::no_argument),
   debuglevel(std::string("--debug,--debuglevel"), 0,
		       std::string("set debug level"),
		       false, Utilities::requires_argument),
   timingon(std::string("--timingon"), false,
		       std::string("turn timing on"),
		       false, Utilities::no_argument),
   help(std::string("-h,--help"), false,
		    std::string("display this message"),
		    false, Utilities::no_argument),
   spatialdatafile(std::string("--sdf,--spatialdatafile"), std::string(""),
			  std::string("spatial map data file"),
		     true, Utilities::requires_argument),
   epiexampledatafile(std::string("--edf,--epiexampledatafile"), std::string(""),
			  std::string("example epi data file"),
		     false, Utilities::requires_argument),
   maskfile(std::string("-m,--mask"), std::string(""),
			  std::string("mask file"),
		     true, Utilities::requires_argument),
   logdir(std::string("-l,--ld,--logdir"), std::string("logdir"),
			  std::string("log directory"),
		     false, Utilities::requires_argument),
   nonspatial(std::string("--ns,--nonspatial"), false,
	   std::string("Nonspatial mixture model"),
	   false, Utilities::no_argument),
   fixmrfprec(std::string("--fmp,--fixmrfprec"), false,
	   std::string("Fix MRF precision to mrfprecstart throughout"),
	   false, Utilities::no_argument),
   mrfprecstart(std::string("--mps,--mrfprecstart"), 10,
	   std::string("MRF precision initial value (default is 10)"),
	   false, Utilities::requires_argument),
   mrfprecmultiplier(std::string("--mpm,--mrfprecmultiplier"), -1,
	   std::string("Update multiplier for MRF precision (default is -1, do not multipy)"),
	   false, Utilities::requires_argument),
   initmultiplier(std::string("--im,--initmultiplier"), 0.3,
	   std::string("Init multiplier (default is 0.3)"),
	   false, Utilities::requires_argument),
   updatetheta(std::string("--nut,--noupdatetheta"), true,
	       std::string("Turn off updating of distribution parameters after non-spatial fit"),false, Utilities::no_argument),
   zfstatmode(std::string("--zfstatmode"), false,
	      std::string("Turn on zfstat mode - this enforces no deactivation class"),false, Utilities::no_argument),
   phi(std::string("--phi"), 0.05,
       std::string("phi (default is 0.05)"),
       false, Utilities::requires_argument),
   niters(std::string("--ni,--niters"), -1,
	std::string("niters (default is -1: auto stop)"),
	false, Utilities::requires_argument),
   threshold(std::string("--th,--thresh"), 0.5,
	std::string("threshold for use when displaying classification maps in MM.html report (default is 0.5, -1 indicates no thresholding)"),
	false, Utilities::requires_argument),
   options("mm", "mm -d <filename>\nmm --verbose\nmm --mask=<mask> --sdf=<filename> --logdir=<logdir>")
   {
     try {
       options.add(verbose);
       options.add(debuglevel);
       options.add(timingon);
       options.add(help);
       options.add(spatialdatafile);
       options.add(epiexampledatafile);
       options.add(maskfile);
       options.add(logdir);
       options.add(nonspatial);
       options.add(fixmrfprec);
       options.add(mrfprecstart);
       options.add(mrfprecmultiplier);
       options.add(initmultiplier);
       options.add(updatetheta);
       options.add(zfstatmode);
       options.add(phi);
       options.add(niters);
       options.add(threshold);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
 std::cerr << e.what() << std::endl;
     }

   }
}

#endif
